using Microsoft.Extensions.DependencyInjection;
using Moq;
using System.Threading.Channels;
using CsharpWMS.Core.Application.Mailing;
using CsharpWMS.Core.Application.BackgroundServices;

[assembly: Parallelize(Workers = 3, Scope = ExecutionScope.MethodLevel)]
namespace CsharpWMS.UnitTests.Core.Application.BackgroundServices
{
    [TestClass]
    public class EmailSenderBackgroundTaskTest
    {
        Mock<IEmailService> _emailService = new Mock<IEmailService>();
        Mock<IServiceScopeFactory> _serviceScopeFactory;
        private readonly int msToWait = 10000;

        public EmailSenderBackgroundTaskTest()
        {
            // necessary mocks for DI
            var serviceProvider = new Mock<IServiceProvider>();
            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);
            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory
                .Setup(x => x.CreateScope())
                .Returns(serviceScope.Object);
            serviceProvider
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);
            
            // channel to inject
            var unboundedChannel = Channel.CreateUnbounded<Email>(
                new UnboundedChannelOptions() { SingleReader = true }
            );
            serviceProvider
                .Setup(x => x.GetService(typeof(Channel<Email>)))
                .Returns(
                    unboundedChannel
                );
            serviceProvider
                .Setup(x => x.GetService(typeof(ChannelReader<Email>)))
                .Returns(
                    unboundedChannel.Reader
                );
            serviceProvider
                .Setup(x => x.GetService(typeof(ChannelWriter<Email>)))
                .Returns(
                    unboundedChannel.Writer
                );
            serviceProvider
                .Setup(x => x.GetService(typeof(IEmailService))).Verifiable();
        }

        [TestMethod]
        public async Task SendEmailThroughtChannel_CallEmailService_SendMail()
        {
            // arrange
            var unboundedChannel = Channel.CreateUnbounded<Email>(
                new UnboundedChannelOptions() { SingleReader = true }
            );
            var emailService = new Mock<IEmailService>();
            var mail = new Email() {
                Body = "test",
                Subject= "test",
                ToAddress = "test@test.test"
            };
            emailService.Setup(a => a.SendMail(mail)).Verifiable();
            var backgroundService = new EmailSenderBackgroundService(unboundedChannel.Reader, emailService.Object);
            Task.Run(
                async () => {
                    await backgroundService.StartAsync(new CancellationToken());
                }
            );

            // act
            await unboundedChannel.Writer.WriteAsync(mail);
            await Task.Delay(msToWait);

            // assert
            this._emailService.Verify();
            // this._warehouseOwnedRepository.Verify(rep => rep.Insert(It.IsNotNull<WarehouseOwned>()));

            backgroundService.Dispose();
        }

        [TestMethod]
        public async Task SendEmailThroughtChannelAfterCancelServiceByToken_DoNotCallEmailService_SendMail()
        {
            // arrange
            var unboundedChannel = Channel.CreateUnbounded<Email>(
                new UnboundedChannelOptions() { SingleReader = true }
            );
            var emailService = new Mock<IEmailService>();
            var mail = new Email() {
                Body = "test",
                Subject= "test",
                ToAddress = "test@test.test"
            };
            emailService.Setup(a => a.SendMail(mail)).Verifiable();
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;
            var backgroundService = new EmailSenderBackgroundService(unboundedChannel.Reader, emailService.Object);
            Task.Run(
                async () => {
                    await backgroundService.StartAsync(token);
                }
            );
            source.Cancel();


            // act
            await unboundedChannel.Writer.WriteAsync(mail);
            await Task.Delay(msToWait);

            // assert
            this._emailService.Verify(a => a.SendMail(mail), Times.Never());
            // Note that we don't want to catch exception. We expect this behavior, this is why we use background service. Else, let's use synchrone execution flow

            backgroundService.Dispose();
        }
        
    }
}