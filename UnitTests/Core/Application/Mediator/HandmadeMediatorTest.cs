using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Application.Mediator.Handlers;
using CsharpWMS.MockForTests.Application;
using Moq;
using Moq.Protected;

namespace CsharpWMS.UnitTests.Core.Application.Mediator
{
    [TestClass]
    public class HandmadeMediatorTest
    {
        public HandmadeMediatorTest()
        {
            
        }

        [TestMethod]
        public void Register_FailIfHandlerIsNull()
        {
            // arrange
            var mediator = new HandmadeMediator();
            INotificationHandler notificationHandler = null;

            // act & assert
            Assert.ThrowsException<ArgumentNullException>(() => {
                mediator.Subscribe(NotificationType.ReceivedBuy, notificationHandler);
            });
        }
        
        [TestMethod]
        public void Register_OnNewNotificationTypeWorks()
        {
            // arrange
            var notificationHandler = new Mock<INotificationHandler>();
            var dico = new Dictionary<NotificationType, List<INotificationHandler>>();
            var mediator = new Mock<HandmadeMediator>();
            mediator.Protected<HandmadeMediator>().SetupGet<Dictionary<NotificationType, List<INotificationHandler>>>("SubscriberRegistre").Returns(dico);
            var notifType = NotificationType.PaiedBuy;
            
            // act
            mediator.Object.Subscribe(notifType, notificationHandler.Object);

            // assert
            Assert.IsTrue(dico.ContainsKey(notifType));
        }
        
        [TestMethod]
        public void Register_OnExistingNotificationTypeWorks()
        {
            // arrange
            var notificationHandler1 = new Mock<INotificationHandler>();
            var notificationHandler2 = new FakeNotificationHandler();
            var notifType = NotificationType.PaiedBuy;
            var dico = new Dictionary<NotificationType, List<INotificationHandler>>();
            dico.Add(notifType, new List<INotificationHandler>(){ notificationHandler1.Object });
            var mediator = new Mock<HandmadeMediator>();
            mediator.Protected<HandmadeMediator>().SetupGet<Dictionary<NotificationType, List<INotificationHandler>>>("SubscriberRegistre").Returns(dico);
            
            // act
            mediator.Object.Subscribe(notifType, notificationHandler2);

            // assert
            Assert.IsTrue(dico[notifType].Contains(notificationHandler2));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Register_TwiceTheSameHandlerInstanceOnNotificationHandlerThrowException()
        {
            // arrange
            var notificationHandler1 = new Mock<INotificationHandler>();
            var notifType = NotificationType.PaiedBuy;
            var dico = new Dictionary<NotificationType, List<INotificationHandler>>();
            dico.Add(notifType, new List<INotificationHandler>(){ notificationHandler1.Object });
            var mediator = new Mock<HandmadeMediator>();
            mediator.Protected<HandmadeMediator>().SetupGet<Dictionary<NotificationType, List<INotificationHandler>>>("SubscriberRegistre").Returns(dico);
            
            // act & assert
            mediator.Object.Subscribe(notifType, notificationHandler1.Object);
        }
        
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Register_TwiceTheSameHandlerTypeOnNotificationHandlerThrowException()
        {
            // arrange
            var notificationHandler1 = new Mock<INotificationHandler>();
            var notificationHandler2 = new Mock<INotificationHandler>();
            var notifType = NotificationType.PaiedBuy;
            var dico = new Dictionary<NotificationType, List<INotificationHandler>>();
            dico.Add(notifType, new List<INotificationHandler>(){ notificationHandler1.Object });
            var mediator = new Mock<HandmadeMediator>();
            mediator.Protected<HandmadeMediator>().SetupGet<Dictionary<NotificationType, List<INotificationHandler>>>("SubscriberRegistre").Returns(dico);
            
            // act & assert
            mediator.Object.Subscribe(notifType, notificationHandler2.Object);
        }
    }
}