using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Core.Domain.Mailing;

namespace CsharpWMS.UnitTests.Core.Mailing
{
    [TestClass]
    public class BuyReceivedEmailBuilderTest
    {
        
        public BuyReceivedEmailBuilderTest()
        {
            
        }

        [TestMethod]
        public async Task BuildEmail_NotEmpty()
        {
            // arrange
            var buy = new Buy()
            {
                DateOfBuy = DateTime.Now,
                PriceBuy = 200,
                Quantity = 3,
                ProductId = 1,
                Product = new Product {
                    Name = "Truc",
                    Sku = "truc2",
                    PriceSell = 400
                },
                WarehouseId = 1,
                Warehouse = new Warehouse {
                    Name = "warehouse nantes",
                }
            };
            BuyReceivedEmailBuilder builder = new BuyReceivedEmailBuilder(buy);

            // act
            var email = await builder.BuildEmail();

            // assert
            Assert.IsTrue(email.Body != null && email.Body.Length > 0, "Body null or empty string");
            Assert.IsTrue(email.Subject != null && email.Subject.Length > 0, "Subject null or empty string");
            Assert.IsTrue(email.ToAddress != null && email.ToAddress.Length > 0, "ToAddress null or empty string");
        }
    }
}