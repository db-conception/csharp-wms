using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Core.Services;
using CsharpWMS.Infrastructure.Data;
using CsharpWMS.Infrastructure.Repositories;
using CsharpWMS.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System.Linq.Expressions;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Application.Specifications;

namespace CsharpWMS.UnitTests.Services
{
    [TestClass]
    public class WarehouseServiceTest
    {
        private readonly DbContextOptions<AppDbContext> _contextOptions;
        private readonly AppDbContext _context;

        IWarehouseUseCases _warehouseUseCases;
        Mock<IRepository<WarehouseOwned>> _warehouseOwnedRepository = new Mock<IRepository<WarehouseOwned>>();
        Mock<IServiceScopeFactory> _serviceScopeFactory;
        public WarehouseServiceTest()
        {
            this. _warehouseOwnedRepository = new Mock<IRepository<WarehouseOwned>>(); 
            // this._context = new AppDbContext(new DbContextOptions<AppDbContext>(Read));

            var serviceProvider = new Mock<IServiceProvider>();
            // serviceProvider
            //     .Setup(x => x.GetService(typeof(ConfigurationDbContext)))
            //     .Returns(new ConfigurationDbContext(Options, StoreOptions));

            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);

            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory
                .Setup(x => x.CreateScope())
                .Returns(serviceScope.Object);

            serviceProvider
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);
            serviceProvider
                .Setup(x => x.GetService(typeof(IRepository<WarehouseOwned>)))
                .Returns(this._warehouseOwnedRepository.Object);
            serviceProvider
                .Setup(x => x.GetService(typeof(ISpecification<WarehouseOwned>)))
                .Returns(new WarehouseOwnedSpecification(any => true));

            var builder = Host.CreateDefaultBuilder()
            .ConfigureServices(services => {
                services.AddScoped<IRepository<WarehouseOwned>>(x => this._warehouseOwnedRepository.Object);
                
            }).Build();

            var isNull = this._warehouseOwnedRepository.Object == null ? "null" : "not null";
            Console.WriteLine($"--> Service / is {isNull}");
            Console.WriteLine($"--> Service / hash code {this._warehouseOwnedRepository.Object.GetHashCode()}");

            
            // this._warehouseOwnedRepository = builder.Services.CreateScope().ServiceProvider
            //                                      .GetService<IRepository<WarehouseOwned>>();
            
            //InfrastructureBuilder.InjectInfrastructureServices(builder)
            this._warehouseUseCases = new WarehouseUseCases(serviceScopeFactory.Object);
        
        }

        [TestMethod]
        public void AddStockToWarehouse_NoAlreadyInStock()
        {
            // arrange
            this._warehouseOwnedRepository.Setup(a => a.Insert(It.IsNotNull<WarehouseOwned>())).Verifiable();
            
            // act
            this._warehouseUseCases.AddStockToWarehouse(1, 1, 50);

            // assert
            this._warehouseOwnedRepository.Verify();
            // this._warehouseOwnedRepository.Verify(rep => rep.Insert(It.IsNotNull<WarehouseOwned>()));

        }
        
        [TestMethod]
        public void AddStockToWarehouse_AlreadyInStock()
        {
            // arrange
            int initalQuantity = 10;
            int toAddQuantity = 50;
            int expectedQuantity = initalQuantity + toAddQuantity;
            this._warehouseOwnedRepository.Setup(a => a.Update(It.IsAny<WarehouseOwned>())).Verifiable();
            // this._warehouseOwnedRepository.Protected().SetupGet<AppDbContext>("_dbContext").Returns(this._context);
            var existingWarehouseOwned = new WarehouseOwned(){
                OwnedId = 1,
                WarehouseId = 1,
                Quantity = initalQuantity
            };
            this._warehouseOwnedRepository.Setup(a => a.FirstOrDefault(It.IsAny<ISpecification<WarehouseOwned>>()))
                .Returns(()=> {
                    System.Console.WriteLine($"--> Mock of firstOrDefault");
                    return existingWarehouseOwned;
                });
                // .Verifiable();
            // this._context.Add<WarehouseOwned>(existingWarehouseOwned);
            
            // act
            this._warehouseUseCases.AddStockToWarehouse(1, 1, toAddQuantity);

            // assert
            this._warehouseOwnedRepository.Verify();
            // Assert.AreEqual(expectedQuantity, existingWarehouseOwned.Quantity);
            // this._warehouseOwnedRepository.Verify(rep => rep.Insert(It.IsNotNull<WarehouseOwned>()));

        }
    }
}