namespace CsharpWMS.Core.Domain.UseCases
{
    public interface IWarehouseUseCases
    {
        void AddStockToWarehouse(int ownedId, int warehouseId, int stock);
    }
}