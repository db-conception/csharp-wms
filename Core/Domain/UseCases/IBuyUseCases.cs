using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Domain.UseCases
{
    public interface IBuyUseCases
    {
        Task ReceiveBuy(Buy buy);


    }
}