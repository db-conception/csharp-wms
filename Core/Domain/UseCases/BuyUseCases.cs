using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Domain.UseCases
{
    public class BuyUseCases : IBuyUseCases
    {
        private readonly IMediator mediator;
        private readonly IRepository<Buy> buyRepository;

        public BuyUseCases(IMediator mediator, IRepository<Buy> buyRepository)
        {
            this.mediator = mediator;
            this.buyRepository = buyRepository;
        }

        public async Task ReceiveBuy(Buy buy)
        {
            buy.IsReceived = true;
            this.buyRepository.Update(buy);
            this.mediator.Notify(this, NotificationType.ReceivedBuy, buy);
        }


    }
}