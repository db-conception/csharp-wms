using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Application.Specifications;

namespace CsharpWMS.Core.Services
{
    public class WarehouseUseCases : IWarehouseUseCases
    {
        private readonly IServiceScopeFactory serviceScopeFactory;

        public WarehouseUseCases(IServiceScopeFactory serviceScopeFactory)
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }

        /*
         * param @stock : the quantity TO ADD, not the new quantity !
         *
         */
        public void AddStockToWarehouse(int ownedId, int warehouseId, int stock)
        {
            using (var scope = this.serviceScopeFactory.CreateScope())
            {
                var warehouseOwnedRepo = scope.ServiceProvider.GetService<IRepository<WarehouseOwned>>();
                var warehouseOwnedSpec = new WarehouseOwnedSpecification(any => any.OwnedId == ownedId && any.WarehouseId == warehouseId);
                var existingWarehouseOwned = warehouseOwnedRepo.FirstOrDefault(warehouseOwnedSpec);
                if(existingWarehouseOwned == null)
                {
                    System.Console.WriteLine("--> Let's add stock to the warhouse ! ");
                    warehouseOwnedRepo.Insert(
                        new WarehouseOwned()
                        {
                            OwnedId = ownedId,
                            WarehouseId = warehouseId,
                            Quantity = stock
                        }
                    );
                }
                else {
                    System.Console.WriteLine("--> Let's update stock in the warhouse ! ");
                    existingWarehouseOwned.Quantity += stock;
                    warehouseOwnedRepo.Update(existingWarehouseOwned);
                }
            }
        }
    }
}