using System.ComponentModel.DataAnnotations;

namespace CsharpWMS.Core.Domain.Models
{
    public class Warehouse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<WarehouseOwned> WarehouseOwneds { get; set; }
    }
}