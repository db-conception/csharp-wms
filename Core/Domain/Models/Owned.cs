namespace CsharpWMS.Core.Domain.Models
{
    public abstract class Owned : IOwned
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Descriminator { get; set; }
    }
}