using System.ComponentModel.DataAnnotations.Schema;

namespace CsharpWMS.Core.Domain.Models
{
    /*
     * Represente something that a warehouse can own. Product for exemple
     */
    public interface IOwned
    {
        string Name { get; set; }

        // you don't find a Stock property here because the stock information is stored on the entity who own it
    }
}