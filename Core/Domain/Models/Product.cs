using System.ComponentModel.DataAnnotations;

namespace CsharpWMS.Core.Domain.Models
{
    public class Product : Owned
    {
        public string Sku { get; set; } 

        public double PriceSell { get; set; }

    }
}