using System.ComponentModel.DataAnnotations;

namespace CsharpWMS.Core.Domain.Models
{
    public class WarehouseOwned
    {
        public int Id { get; set; }

        public int WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }

        public int OwnedId { get; set; }

        public Owned Owned { get; set; }

        public int Quantity { get; set; }
    }
}