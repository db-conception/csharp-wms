using System.ComponentModel.DataAnnotations;

namespace CsharpWMS.Core.Domain.Models
{
    public class Buy
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public Product Product { get; set; }

        public int WarehouseId { get; set; }
        
        public Warehouse Warehouse { get; set; }

        public bool IsPaid { get; set; }    

        public DateTime DateOfBuy { get; set; }

        public double PriceBuy { get; set; }

        public bool IsReceived { get; set; }

        public int Quantity { get; set; }
    }
}