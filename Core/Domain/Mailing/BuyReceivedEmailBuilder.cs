using CsharpWMS.Core.Application.Mailing;
using Email = CsharpWMS.Core.Application.Mailing.Email;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Domain.Mailing
{
    public class BuyReceivedEmailBuilder : IEmailBuilder
    {
        private readonly Buy buy;

        public BuyReceivedEmailBuilder(Buy buy)
        {
            this.buy = buy;
        }
        public Task<Email> BuildEmail()
        {
            // TODO TU : deal with case if buy.Product is null
            string body = $"Hi, \n\nWe well received {buy?.Product?.Name} x {buy.Quantity}\n\nCsharpWMS";
            var email = new Email()
            {
                Body = body,
                ToAddress = "admin@company.com",
                Subject = "Buy received ! "
            };

            return Task.FromResult(email);
        }
    }
}