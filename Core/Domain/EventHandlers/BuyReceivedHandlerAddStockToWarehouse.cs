using CsharpWMS.Core.Application.Mediator.Handlers;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Domain.EventHandlers
{
    public class BuyReceivedHandlerAddStockToWarehouse : DomainHandler<Buy>
    {
        private readonly IWarehouseUseCases warehouseUseCases;

        public BuyReceivedHandlerAddStockToWarehouse(IWarehouseUseCases warehouseUseCases)
        {
            this.warehouseUseCases = warehouseUseCases;
        }
        public override async void HandleAndProcess(Buy buy)
        {
            System.Console.WriteLine("--> AddStockToWarehouseHandler : Data handled ! ");

            this.warehouseUseCases.AddStockToWarehouse(buy.ProductId, buy.WarehouseId, buy.Quantity);
        }
    }
}