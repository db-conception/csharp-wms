using System.Threading.Channels;
using CsharpWMS.Core.Application.Mailing;
using CsharpWMS.Core.Application.Mediator.Handlers;
using CsharpWMS.Core.Domain.Mailing;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Domain.EventHandlers
{
    public class BuyReceivedHandlerSendEmail : DomainHandler<Buy>
    {
        private readonly IEmailService emailService;
        private readonly ChannelWriter<Email> channelWriter;
        public BuyReceivedHandlerSendEmail(
            IEmailService emailService,
            ChannelWriter<Email> channelWriter)
        {
            if(emailService == null)
            {
                System.Console.WriteLine($"--> Email service is null");
            }
            this.emailService = emailService;
            this.channelWriter = channelWriter;
        }
        public override async void HandleAndProcess(Buy buy)
        {
            System.Console.WriteLine("--> EmailToSendHandler : Data handled ! ");
            if(buy == null)
            {
                System.Console.WriteLine("--> buy is null");
                throw new ArgumentNullException("buy");
            }
            if(this.emailService == null)
            {
                System.Console.WriteLine("--> email service is null");
            }

            // Bad way to do async
            // Task.Run(async () => {
            //     await this.emailService.SendMail(
            //         "test@csharpwms.com",
            //         $"Hey ! We received the product {buy.Product.Name} at the warehouse {buy.Warehouse.Name}",
            //         $"We well receive {buy.Quantity} x {buy.Product.Name} at {buy.Warehouse.Name}"
            //     );
            // }); 


            // Synchrone Way
            // System.Console.WriteLine("--> before call email service");
            // this.emailService.SendMail(
            //     "test@csharpwms.com",
            //     "you",
            //     "you"
            //     // $"Hey ! We received the product {buy.Product.Name} at the warehouse {buy.Warehouse.Name}",
            //     // $"We well receive {buy.Quantity} x {buy.Product.Name} at {buy.Warehouse.Name}"
            // );

            // send data throught channel to hostedService
            var buyReceivedEmailBuilder = new BuyReceivedEmailBuilder(buy);
            var mailToSend = await buyReceivedEmailBuilder.BuildEmail();
            await this.channelWriter.WriteAsync(mailToSend);

        }
    }
}