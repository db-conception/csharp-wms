namespace CsharpWMS.Core.Application.Mailing
{
    public interface IEmailBuilder{

        Task<Email> BuildEmail();

    }
}