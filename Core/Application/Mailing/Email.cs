namespace CsharpWMS.Core.Application.Mailing
{
    public class Email{
        public string ToAddress { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}