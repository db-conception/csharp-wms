using CsharpWMS.Core.Application.Mailing;

namespace CsharpWMS.Core.Application.Mailing
{
    public interface IEmailService
    {
        void SendMail(string toAddress, string subject, string body);
        void SendMail(Email email);
    }
}