using System.Linq.Expressions;
using CsharpWMS.Core.Application.Specifications;

namespace CsharpWMS.Core.Application.Repositories
{
    public interface IRepository<T>
    {
        T GetById(int id);

        T FirstOrDefault(ISpecification<T> spec);

        IEnumerable<T> List();

        IEnumerable<T> List(Expression<Func<T, bool>> predicate);

        IEnumerable<T> List(ISpecification<T> spec);
        
        void Insert(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}