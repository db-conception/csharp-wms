namespace CsharpWMS.Core.Application.Repositories
{
    public interface ITransactionBuilder
    {
        void BeginTransaction(string? name = null);
        void CommitTransaction(string? name = null);
        void RollbackTransaction(string? name = null);
    }
}