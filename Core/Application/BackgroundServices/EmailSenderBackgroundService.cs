using System.Threading.Channels;
using CsharpWMS.Core.Application.Mailing;

namespace CsharpWMS.Core.Application.BackgroundServices
{
    public class EmailSenderBackgroundService : BackgroundService
    {
        private readonly ChannelReader<Email> channelReader;
        private readonly IEmailService emailService;

        public EmailSenderBackgroundService(ChannelReader<Email> channelReader, IEmailService emailService)
        {
            this.channelReader = channelReader;
            this.emailService = emailService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Email mailToSend;
            System.Console.WriteLine($"--> EmailSenderBackgroundService - processing");
            
            while(!stoppingToken.IsCancellationRequested)
            {
                // We unlock the thread during this 4 secondes
                await Task.Delay(4000);
                // We are listening the channel
                if(this.channelReader.TryRead(out mailToSend)){
                    Console.WriteLine($"--> EmailSenderBackgroundService > we received a mail to send");
                    await Task.Delay(2000);  // we simulate a long job
                    await this.TheJobToDo(mailToSend);
                }

            }

            System.Console.WriteLine($"--> EmailSenderBackgroundService - stop to listen");
            // return Task.CompletedTask;
        }

        private async Task TheJobToDo(Email emailToSend)
        {
            Console.WriteLine($"--> EmailSenderBackgroundService > Let's use mail service");
            this.emailService.SendMail(emailToSend);
        }
    }
}