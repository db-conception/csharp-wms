using System.Linq.Expressions;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Application.Specifications
{
    public class WarehouseSpecification : BaseSpecification<Warehouse>
    {
        public WarehouseSpecification(Expression<Func<Warehouse, bool>> criteria) : base(criteria)
        {
            
        }
    }
}