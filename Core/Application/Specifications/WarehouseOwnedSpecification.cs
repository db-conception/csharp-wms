using System.Linq.Expressions;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Core.Application.Specifications
{
    public class WarehouseOwnedSpecification : BaseSpecification<WarehouseOwned>
    {
        public WarehouseOwnedSpecification(Expression<Func<WarehouseOwned, bool>> criteria) : base(criteria)
        {
        }

        
    }
}