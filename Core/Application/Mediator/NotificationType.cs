namespace CsharpWMS.Core.Application.Mediator
{
    public enum NotificationType
    {
        ReceivedBuy,
        PaiedBuy,
        ProductOutOfStock,
        ProductInLowStock
    }
}