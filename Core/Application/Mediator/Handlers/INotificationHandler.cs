namespace CsharpWMS.Core.Application.Mediator.Handlers
{
    public interface INotificationHandler
    {
        void HandleAndProcess(object data);
    }
}