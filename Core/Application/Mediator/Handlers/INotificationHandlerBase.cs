namespace CsharpWMS.Core.Application.Mediator.Handlers
{
    public abstract class INotificationHandlerBase<T> : INotificationHandler
    {
        void INotificationHandler.HandleAndProcess(object data)
        {
            this.HandleAndProcess((T)data);
        }

        public abstract void HandleAndProcess(T data);
    }
}