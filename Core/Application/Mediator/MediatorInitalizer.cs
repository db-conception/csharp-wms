
using HandlersImplementation = CsharpWMS.Core.Domain.EventHandlers;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Application.Mailing;

namespace CsharpWMS.Core.Application.Mediator
{
    public static class MediatorInitalizer
    {
        // public static void Initalize(IMediator mediator, AddStockToWarehouseHandler addStockToWarehouseHandler)
        public static void Initalize(IApplicationBuilder app)
        {
            using(var serviceScope = app.ApplicationServices.CreateScope()){
                var mediator = serviceScope.ServiceProvider.GetService<IMediator>();
                var warehouseUseCases = serviceScope.ServiceProvider.GetService<IWarehouseUseCases>();
                var emailService = serviceScope.ServiceProvider.GetService<IEmailService>();
                var buyReceivedHandlerAddStockToWarehouse = serviceScope.ServiceProvider.GetService<HandlersImplementation.BuyReceivedHandlerAddStockToWarehouse>();
                var buyReceivedHandlerSendEmail = serviceScope.ServiceProvider.GetService<HandlersImplementation.BuyReceivedHandlerSendEmail>();

                mediator.Subscribe(NotificationType.ReceivedBuy, buyReceivedHandlerAddStockToWarehouse);
                mediator.Subscribe(NotificationType.ReceivedBuy, buyReceivedHandlerSendEmail);
            }

        }
    }
}