using CsharpWMS.Core.Application.Mediator.Handlers;

namespace CsharpWMS.Core.Application.Mediator
{
    public class HandmadeMediator : IMediator
    {
        protected virtual Dictionary<NotificationType, List<INotificationHandler>> SubscriberRegistre { get; set; } = new Dictionary<NotificationType, List<INotificationHandler>>();

        public void Notify(object sender, NotificationType notificationType, object data)
        {
            System.Console.WriteLine("--> Notify called");
            if(!this.SubscriberRegistre.ContainsKey(notificationType))
            {
                System.Console.WriteLine($"--> Do not contains key {notificationType.ToString()}");
                System.Console.WriteLine($"--> Dico contains {this.SubscriberRegistre.Count}");
                return;
            }

            foreach (var anyHandler in this.SubscriberRegistre[notificationType])
            {
                System.Console.WriteLine("--> notify " + anyHandler.GetType().ToString());
                anyHandler.HandleAndProcess(data);
            }
        }

        public void Subscribe(NotificationType notificationType, INotificationHandler notificationHandler)
        {
            if(notificationType == null)
                throw new ArgumentNullException("notificationType");
            if(notificationHandler == null)
                throw new ArgumentNullException("notificationHandler");
            System.Console.WriteLine("--> Subscribe of " + notificationHandler.GetType().ToString() + " to " + notificationType.ToString());
            
            if(!this.SubscriberRegistre.ContainsKey(notificationType))
            {
                System.Console.WriteLine($"--> Create the key and the dico for {notificationType.ToString()}");
                this.SubscriberRegistre.Add(notificationType, new List<INotificationHandler>());
            }

            System.Console.WriteLine($"--> {notificationHandler.GetType().ToString()}");
            if(this.SubscriberRegistre[notificationType].Select(handler => handler.GetType().ToString()).Contains(notificationHandler.GetType().ToString())){
                throw new Exception($"An instance of the class '{notificationHandler.GetType().ToString()}' is already subscribed to notification type {notificationType.ToString()}");
            }

            System.Console.WriteLine($"--> Add the handler instance {notificationHandler.GetType().ToString()} to the key {notificationType.ToString()}");
            this.SubscriberRegistre[notificationType].Add(notificationHandler);
        }
    }
}