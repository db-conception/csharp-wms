using CsharpWMS.Core.Application.Mediator.Handlers;

namespace CsharpWMS.Core.Application.Mediator
{
    public interface IMediator
    {
        void Notify(object sender, NotificationType notificationType, object data);

        void Subscribe(NotificationType notificationType, INotificationHandler notificationHandler);
    }
}