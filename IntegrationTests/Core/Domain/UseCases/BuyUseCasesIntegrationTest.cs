using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Domain.EventHandlers;
using CsharpWMS.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CsharpWMS.MockForTests.Application;
using CsharpWMS.Core.Services;
using System.Threading.Channels;
using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Application.Mailing;
using CsharpWMS.Core.Application.BackgroundServices;
using CsharpWMS.Core.Application.Specifications;
using CsharpWMS.Core.Application.Repositories;

[assembly: Parallelize(Workers = 3, Scope = ExecutionScope.MethodLevel)]
namespace CsharpWMS.Integration.Services
{
    [TestClass]
    public class BuyUseCasesIntegrationTest
    {
        private readonly DbContextOptions<AppDbContext> _contextOptions;
        private readonly Mock<AppDbContext> _context;

        IBuyUseCases _buyUseCase;
        IMediator _mediator = new HandmadeMediator();
        // Mock<IMediator> _mediator = new Mock<IMediator>();
        Mock<IRepository<Buy>> _buyRepository = new Mock<IRepository<Buy>>();
        Mock<IRepository<WarehouseOwned>> _warehouseOwnedRepo = new Mock<IRepository<WarehouseOwned>>();
        Mock<IWarehouseUseCases> _warehouseService = new Mock<IWarehouseUseCases>();
        Mock<IServiceScopeFactory> _serviceScopeFactory;
        Mock<IServiceScope> _serviceScope;
        Mock<IServiceProvider> _serviceProvider;
        public BuyUseCasesIntegrationTest()
        {
            this._contextOptions = new DbContextOptions<AppDbContext>();
            this._context = new Mock<AppDbContext>(this._contextOptions);
            _serviceProvider = new Mock<IServiceProvider>();
            _serviceProvider
                .Setup(x => x.GetService(typeof(AppDbContext)))
                .Returns(this._context.Object);

            _serviceScope = new Mock<IServiceScope>();
            _serviceScope.Setup(x => x.ServiceProvider).Returns(_serviceProvider.Object);

            _serviceScopeFactory = new Mock<IServiceScopeFactory>();
            _serviceScopeFactory
                .Setup(x => x.CreateScope())
                .Returns(_serviceScope.Object);
            _serviceProvider
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(_serviceScopeFactory.Object);
            _serviceProvider
                .Setup(x => x.GetService(typeof(IRepository<Buy>)))
                .Returns(this._buyRepository.Object);
            _serviceProvider
                .Setup(x => x.GetService(typeof(IRepository<WarehouseOwned>)))
                .Returns(this._warehouseOwnedRepo.Object);
            _serviceProvider
                .Setup(x => x.GetService(typeof(IMediator)))
                .Returns(this._mediator);
            _serviceProvider
                .Setup(x => x.GetService(typeof(IWarehouseUseCases)))
                .Returns(this._warehouseService.Object);

            var builder = Host.CreateDefaultBuilder()
            .ConfigureServices(services => {
                services.AddScoped<IRepository<Buy>>(x => this._buyRepository.Object);
            }).Build();

            // this._buyUseCase = _serviceProvider.Object.GetService<BuyUseCases>();
            this._buyUseCase = new BuyUseCases(_mediator, _buyRepository.Object);

        }

        [TestMethod]
        public void ReceiveBuy_TriggerBuyReceivedEvent()
        {
            // arrange
            var buy = new Buy()
            {
                Id = 1,
                DateOfBuy = DateTime.Now,
                ProductId = 2,
                WarehouseId = 2,
                IsReceived = false,
                IsPaid = true,
                Quantity = 50
            };
            // this._context.Object.Buys.Add(buy);
            var handler = new Mock<FakeNotificationHandler>();
            handler.Setup( h => h.HandleAndProcess(It.IsNotNull<object>())).Verifiable();
            this._mediator.Subscribe(NotificationType.ReceivedBuy, handler.Object);
            
            // act
            this._buyUseCase.ReceiveBuy(buy);

            // assert
            handler.Verify();
        }
        
        [TestMethod]
        public void ReceiveBuy_AddProductToWareHouse()
        {
            // arrange
            var buy = new Buy()
            {
                Id = 1,
                DateOfBuy = DateTime.Now,
                ProductId = 2,
                WarehouseId = 2,
                IsReceived = false,
                IsPaid = true,
                Quantity = 50
            };
            var warehouseOwned = new WarehouseOwned()
            {
                Id = 1,
                WarehouseId = 2,
                OwnedId = 2,
                Quantity = 10
            };
            // Subjects of the test should not be mocked. Because we test the liaison of 
            // UseCase->HandmadeMediator->BuyReceivedHandlerAddStockToWarehouse->WarehouseService (will be renamed use case too)
            // Neither of thoses instances are mocked. However, we mock repositories used in services and use cases
            int expectedFinalQuantityInWarehouse = buy.Quantity + warehouseOwned.Quantity;
            var realWarehouseService = new WarehouseUseCases(this._serviceScopeFactory.Object);
            this._serviceProvider.Setup(sp => sp.GetService(typeof(IWarehouseUseCases))).Returns(realWarehouseService);
            var handler = new BuyReceivedHandlerAddStockToWarehouse(realWarehouseService);
            this._warehouseOwnedRepo.Setup(wor => wor.FirstOrDefault(It.IsAny<WarehouseOwnedSpecification>())).Returns(warehouseOwned);
            this._warehouseService.Setup(ws => ws.AddStockToWarehouse(buy.ProductId, buy.WarehouseId, buy.Quantity)).Verifiable();
            this._mediator.Subscribe(NotificationType.ReceivedBuy, handler);
            
            // act
            this._buyUseCase.ReceiveBuy(buy);

            // assert
            Assert.IsTrue(buy.IsReceived);
            Assert.AreEqual(expectedFinalQuantityInWarehouse, warehouseOwned.Quantity);
        }

        [TestMethod]
        public async Task ReceiveBuy_SendEmail()
        {
            // Here our goal is to test that the mail service is well used by the backgroud service
            // after sending a message throught the channel on marking received a buy 

            // arrange
            var buy = new Buy()
            {
                Id = 1,
                DateOfBuy = DateTime.Now,
                ProductId = 2,
                WarehouseId = 2,
                IsReceived = false,
                IsPaid = true,
                Quantity = 50
            };
            var warehouseOwned = new WarehouseOwned()
            {
                Id = 1,
                WarehouseId = 2,
                OwnedId = 2,
                Quantity = 10
            };
            // Subjects of the test should not be mocked. Because we test the liaison of 
            // UseCase->HandmadeMediator->BuyReceivedHandlerAddStockToWarehouse->WarehouseService (will be renamed use case too) TODO EDIT
            // Neither of thoses instances are mocked. However, we mock repositories used in services and use cases
            int expectedFinalQuantityInWarehouse = buy.Quantity + warehouseOwned.Quantity;
            var realWarehouseService = new WarehouseUseCases(this._serviceScopeFactory.Object);
            this._serviceProvider.Setup(sp => sp.GetService(typeof(IWarehouseUseCases))).Returns(realWarehouseService);
            var unboundedChannel = Channel.CreateUnbounded<Email>(
                new UnboundedChannelOptions() { SingleReader = true }
            );
            this._serviceProvider.Setup(sp => sp.GetService(typeof(ChannelReader<Email>))).Returns(unboundedChannel.Reader);
            this._serviceProvider.Setup(sp => sp.GetService(typeof(ChannelWriter<Email>))).Returns(unboundedChannel.Writer);
            this._serviceProvider.Setup(sp => sp.GetService(typeof(IWarehouseUseCases))).Returns(realWarehouseService);
            var emailService = new Mock<IEmailService>();
            this._serviceProvider.Setup(x => x.GetService(typeof(IEmailService))).Returns(emailService.Object);
            // emailService.Setup(es => es.SendMail(It.IsAny<Email>())).Verifiable();
            var handler = new BuyReceivedHandlerSendEmail(emailService.Object, unboundedChannel.Writer);
            this._warehouseOwnedRepo.Setup(wor => wor.FirstOrDefault(It.IsAny<WarehouseOwnedSpecification>())).Returns(warehouseOwned);
            this._warehouseService.Setup(ws => ws.AddStockToWarehouse(buy.ProductId, buy.WarehouseId, buy.Quantity)).Verifiable();
            this._mediator.Subscribe(NotificationType.ReceivedBuy, handler);
            var backgroundService = new EmailSenderBackgroundService(unboundedChannel.Reader, emailService.Object);
            Task.Run(
                async () => {
                    await backgroundService.StartAsync(new CancellationToken());
                }
            );
            var msToWait = 10000;

            // act
            await this._buyUseCase.ReceiveBuy(buy);
            await Task.Delay(msToWait);

            // assert
            emailService.Verify(es => es.SendMail(It.IsAny<Email>()), Times.Once());
            backgroundService.Dispose();
        }
    }
}