using System.Linq.Expressions;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Application.Specifications;

namespace CsharpWMS.IntegrationTests.Repositories
{
    public class FakeRepository<T> : IRepository<T> where T : class
    {
        public IEnumerable<T> Data { get; set; } = new List<T>();
        public void Delete(T entity)
        {
            this.Data = this.Data.Where(x => x != entity);
        }

        public T FirstOrDefault(ISpecification<T> spec)
        {
            throw new NotImplementedException();
            // fetch a Queryable that includes all expression-based includes
            // var queryableResultWithIncludes = spec.Includes
            //     .Aggregate(this.Data,
            //         (current, include) => current.Include(include));

            // modify the List to include any string-based include statements
            // var secondaryResult = spec.IncludeStrings
            //     .Aggregate(queryableResultWithIncludes,
            //         (current, include) => current.Include(include));

            // // return the result of the query using the specification's criteria expression
            // return secondaryResult
            //                 .FirstOrDefault(spec.Criteria);
        }

        public T GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Insert(T entity)
        {
            this.Data = this.Data.Append(entity);
        }

        public IEnumerable<T> List()
        {
            return Data;
        }

        public IEnumerable<T> List(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> List(ISpecification<T> spec)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}