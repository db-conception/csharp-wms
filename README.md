# csharp wms

Just an API for a fake WMS to practice clean architecture in a .net core project

## Getting started
- `git clone `
- Edit "Api\appsettings.Development.json" by setting "ForceInMemDb" property to true or by updating the connection string
- `cd .\Api\`
- `dotnet run`

### Migrations
- `cd .\Api`
- `dotnet ef migrations add GenerateTables --project ..\Infrastructure\Infrastructure.csproj`
- `dotnet ef database update --project ..\Infrastructure\Infrastructure.csproj`

## Test
- `dotnet test -c 'Debug' -v n`

## Features
- List product
- Create product (in database, do not link it to a warehouse)
- Buy product
- List Warehouse
- List Buy (with filtering by paid and received state)
- Create Buy
- Mark Buy as paid
- Mark Buy as received
- Get inventory of a Warehouse (with coming stock, = bought but not delivred stock)
- Email alert on Buy and buy received

## Potentials others features
- Add Raw Material modele, owned by Warehouse
- Craft product by destroying raw material
- 

## API Documentation
1. `cd Api`
1. `dotnet run`
1. Browse `http://localhost:[your port]/swagger`

## Pattern used
### Mediator Design Pattern
As you can see in Core\Mediator\IMediator.cs, it's not exactly a Mediator Design Pattern. Because Mediator Design Pattern treat the relation between business domaines internaly. Here it's more an Obersver Design Pattern or even a Subscribe/Notifier Design Pattern. This way if for me more intuitive and natural. I choose to keep the name of Mediator because may be I will implement the real Mediator design pattern inside.

### Specification Pattern
Look at Core\Specifications\ISpecifications.cs and at ``GetProductOfWarehouseById()`` function of Api\Controllers\WarehousesController.cs

### ...

## MCO
### Add an Event Handler
To add an event handler, create a class inside Core/Mediator/Handlers named '[EVENT-NAME]Handler[HANDLER-NAME]'.

For Exemple '**BuyReceived**Handler**SendEmail**.cs'. The [HANDLER-NAME] must be in infinitive.