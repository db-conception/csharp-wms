using CsharpWMS.Core.Application.Mediator.Handlers;

namespace CsharpWMS.MockForTests.Application
{
    public record FakeNotificationHandler : INotificationHandler
    {
        public virtual void HandleAndProcess(object data)
        {
            throw new NotImplementedException();
        }
    }
}