using Email = CsharpWMS.Core.Application.Mailing.Email;
using CsharpWMS.Core.Application.Mailing;

namespace CsharpWMS.Infrastructure.Mailing
{
    public class FakeEmailService : IEmailService
    {
        public void SendMail(string toAddress, string subject, string body)
        {
            Console.WriteLine($"--> SendMail to {toAddress}, titled {subject}");
            System.Console.WriteLine("Content : \n" + body);
        }

        public void SendMail(Email email)
        {
            this.SendMail(email.ToAddress, email.Subject, email.Body);
        }
    }
}