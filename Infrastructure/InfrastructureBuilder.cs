﻿using System.Reflection;
using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CsharpWMS.Infrastructure;

public static class InfrastructureBuilder
{
    public static void InjectInfrastructureServices(WebApplicationBuilder builder)
    {
        InjectDBContext(builder);
        InjectMediator(builder);
    }

    private static void InjectMediator(WebApplicationBuilder builder)
    {
        // For the moment we inject our handmade mediator
        builder.Services.AddSingleton<IMediator, Core.Application.Mediator.HandmadeMediator>();
        // builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
    }
    
    private static void InjectDBContext(WebApplicationBuilder builder)
    {
        bool forceInMemDb = Boolean.Parse(builder.Configuration["ForceInMemDb"]);
        if (builder.Environment.IsDevelopment() && forceInMemDb)
        {
            System.Console.WriteLine("--> Development connection, force in mem db");
            builder.Services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase("InMem"));
        }
        else if (builder.Environment.IsDevelopment() && !forceInMemDb)
        {
            System.Console.WriteLine("--> Development connection");
            builder.Services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(
                builder.Configuration.GetConnectionString("LocalContextConnection"),
                b => b.MigrationsAssembly("Infrastructure")
            ));
        }
    }
}
