﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddUnicityConstraintOnWarehousAndOwnedId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WarehouseOwneds_OwnedId",
                table: "WarehouseOwneds");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_WarehouseOwneds_OwnedId_WarehouseId",
                table: "WarehouseOwneds",
                columns: new[] { "OwnedId", "WarehouseId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_WarehouseOwneds_OwnedId_WarehouseId",
                table: "WarehouseOwneds");

            migrationBuilder.CreateIndex(
                name: "IX_WarehouseOwneds_OwnedId",
                table: "WarehouseOwneds",
                column: "OwnedId");
        }
    }
}
