using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Infrastructure.Data.Descriminator;
using Microsoft.EntityFrameworkCore;

namespace CsharpWMS.Infrastructure.Data
{
    public class AppDbContext : DbContext{
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt){

        }

        public DbSet<Owned> Owneds { get; set; }

        public DbSet<Warehouse> Warehouses { get; set; }

        public DbSet<WarehouseOwned> WarehouseOwneds { get; set; }

        public DbSet<Buy> Buys { get; set; }
        
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Owned>()
                .UseTphMappingStrategy();
            modelBuilder.Entity<Owned>()
                .HasDiscriminator(o => o.Descriminator)
                .HasValue<Product>((int)OwnedDescriminator.Product);
            modelBuilder.Entity<Owned>()
                .HasKey(ow => ow.Id);
            modelBuilder.Entity<Owned>()
                .Property(ow => ow.Id).IsRequired();
            modelBuilder.Entity<Owned>()
                .Property(ow => ow.Name).IsRequired();
             modelBuilder.Entity<Product>()
                .HasBaseType<Owned>();
             modelBuilder.Entity<Product>()
                .Property(pr => pr.Sku).IsRequired(); // TODO TEST with true SQL SERVER
            modelBuilder.Entity<Product>()
                .Property(pr => pr.PriceSell).IsRequired(); // TODO TEST with true SQL SERVER

            modelBuilder.Entity<Warehouse>()
                .HasKey(wh => wh.Id);
            modelBuilder.Entity<Warehouse>()
                .Property(wh => wh.Id)
                .IsRequired();
            modelBuilder.Entity<Warehouse>()
                .Property(wh => wh.Name)
                .IsRequired();

            modelBuilder.Entity<WarehouseOwned>()
                .HasKey(wo => wo.Id);
            modelBuilder.Entity<WarehouseOwned>()
                .HasAlternateKey(wo => new { wo.OwnedId, wo.WarehouseId});
            modelBuilder.Entity<WarehouseOwned>()
                .Property(wo => wo.Id)
                .IsRequired();
            modelBuilder.Entity<WarehouseOwned>()
                .Property(wo => wo.WarehouseId)
                .IsRequired();
            modelBuilder.Entity<WarehouseOwned>()
                .Property(wo => wo.OwnedId)
                .IsRequired();
            modelBuilder.Entity<WarehouseOwned>()
                .Property(wo => wo.Quantity)
                .IsRequired();

            modelBuilder.Entity<Buy>()
                .HasKey(b => b.Id);
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.Id)
                .IsRequired();
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.ProductId)
                .IsRequired();
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.WarehouseId)
                .IsRequired();
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.DateOfBuy)
                .IsRequired()
                .HasDefaultValueSql("getdate()"); // TODO TEST WITH TRUE SQL SERVER
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.IsPaid)
                .HasDefaultValue(false);
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.IsReceived)
                .HasDefaultValue(false);
            modelBuilder.Entity<Buy>()
                .Property(wo => wo.Quantity)
                .HasDefaultValue(1);
        }
    }
}