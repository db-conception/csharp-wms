using CsharpWMS.Core.Domain.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CsharpWMS.Infrastructure.Data
{
    public static class AppDbContextSeed
    {
        private static readonly bool _isProduction;

        public static void PrepPopulation(IApplicationBuilder app, bool IsProduction){
            using(var serviceScope = app.ApplicationServices.CreateScope()){
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>(), IsProduction);
            }
        }

        private static void SeedData(AppDbContext context, bool IsProduction){

            if(IsProduction){
                Console.WriteLine("--> Attempting to apply migrations...");
                try
                {
                    context.Database.Migrate();
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine($"--> Could not run migrations : {ex.Message}");
                    throw ex;
                }
            }

            if(!context.Owneds.Any()){
                Console.WriteLine("--> Seeding Data...");

                var tshirtBlue = new Product() { Name = "Tshirt", PriceSell = 12, Sku = "tshirt-001-blue" };
                var shortBlue = new Product() { Name = "Short", PriceSell = 20, Sku = "short-001-blue" };
                var jeansBlue = new Product() { Name = "Jeans", PriceSell = 110, Sku = "jeans-001-blue" };
                context.Owneds.AddRange(
                    tshirtBlue,
                    shortBlue,
                    jeansBlue
                );

                context.Warehouses.AddRange(
                    new Warehouse() { 
                        Name = "Warehouse of Nantes",
                        WarehouseOwneds = new List<WarehouseOwned>()
                        {
                            new WarehouseOwned() {
                                Owned = tshirtBlue,
                                Quantity = 10
                            }
                        }
                    },
                    new Warehouse() { Name = "Warehouse of Rennes" }
                );

                
                context.SaveChanges();
            }
            else{
                Console.WriteLine("--> We Already have data");
            }
        }
    }
}