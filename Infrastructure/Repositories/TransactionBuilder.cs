using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Infrastructure.Data;

namespace CsharpWMS.Infrastructure.Repositories
{
    public class TransactionBuilder : ITransactionBuilder
    {
        private readonly AppDbContext context;

        public TransactionBuilder(AppDbContext context)
        {
            this.context = context;
        }

        public void BeginTransaction(string? name = null)
        {
            this.BeginTransaction(name);
        }

        public void CommitTransaction(string? name = null)
        {
            this.CommitTransaction(name);
        }

        public void RollbackTransaction(string? name = null)
        {
            this.RollbackTransaction(name);
        }
    }
}