using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Api.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            // Source -> Target
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();
            CreateMap<ProductCreationDto, Product>();
        }
    }
}