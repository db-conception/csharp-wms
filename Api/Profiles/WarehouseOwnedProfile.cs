using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Api.Profiles
{
    public class WarehouseOwnedProfile : Profile
    {
        public WarehouseOwnedProfile()
        {
            // Source -> Target
            CreateMap<WarehouseOwned, OwnedProductDto>()
                .ForMember(dest => dest.Id, option => option.MapFrom( source => source.Owned.Id))
                .ForMember(dest => dest.Name, option => option.MapFrom( source => source.Owned.Name))
                .ForMember(dest => dest.PriceSell, option => option.MapFrom( source => ((Product)source.Owned).PriceSell))
                .ForMember(dest => dest.Sku, option => option.MapFrom( source => ((Product)source.Owned).Sku))
                .ForMember(dest => dest.Stock, option => option.MapFrom( source => source.Quantity))
                ;
                // .ForAllMembers(option => option.UseDestinationValue());
        }
    }
}