using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Api.Profiles
{
    public class BuyProfile : Profile
    {
        public BuyProfile()
        {
            // Source -> Target
            CreateMap<Buy, BuyReadDto>();
            CreateMap<BuyReadDto, Buy>();
            CreateMap<BuyCreationDto, Buy>();
        }
    }
}