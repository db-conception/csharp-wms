using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Domain.Models;

namespace CsharpWMS.Api.Profiles
{
    public class WarehouseProfile : Profile
    {
        public WarehouseProfile()
        {
            // Source -> Target
            CreateMap<Warehouse, WarehouseDto>();
            CreateMap<WarehouseDto, Warehouse>();
            CreateMap<WarehouseCreationDto, Warehouse>();
            CreateMap<Warehouse, WarehouseAndOwningDto>()
                .ForMember(destination => destination.OwnedProducts, action => {
                    action.MapFrom(source => source.WarehouseOwneds);
                });
        }
    }
}