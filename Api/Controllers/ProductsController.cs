using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Application.Repositories;

namespace CsharpWMS.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductUseCases productUseCases;
        private readonly IRepository<Product> productRepo;
        private readonly IMapper mapper;

        public ProductsController(IRepository<Product> productRepo, IMapper mapper)
        {
            this.productUseCases = productUseCases;
            this.productRepo = productRepo;
            this.mapper = mapper;
        }

        [HttpGet]
        public ActionResult<ProductDto> GetProducts()
        {
            var products = this.productRepo.List();
            var productDtos = this.mapper.Map<IEnumerable<ProductDto>>(products);
            return Ok(productDtos);
        }

        [HttpGet("{id}"), ActionName("Get Product")]
        public ActionResult<ProductDto> GetProduct(int id)
        {
            var product = this.productRepo.GetById(id);
            var productDto = this.mapper.Map<ProductDto>(product);
            return Ok(productDto);
        }

        [HttpPost]
        public ActionResult<ProductDto> CreateProduct([FromBody]ProductCreationDto productCreationDto)
        {
            if(productCreationDto != null)
            {
                var productModel = this.mapper.Map<Product>(productCreationDto);
                this.productRepo.Insert(productModel);
                var productDto = this.mapper.Map<ProductDto>(productModel);
                return CreatedAtAction("Get Product", new { Id = productModel.Id }, productDto);
            }

            return BadRequest();
        }
        
    }
}