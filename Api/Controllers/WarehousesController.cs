using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Application.Specifications;
using CsharpWMS.Core.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace CsharpWMS.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class WarehousesController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRepository<WarehouseOwned> warehouseOwnedRepo;
        private readonly IRepository<Warehouse> warehouseRepo;

        public WarehousesController(
            IMapper mapper,
            IRepository<Warehouse> _warehouseRepo,
            IRepository<WarehouseOwned> warehouseOwnedRepo)
        {
            this.mapper = mapper;
            this.warehouseRepo = _warehouseRepo;
            this.warehouseOwnedRepo = warehouseOwnedRepo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Warehouse>> GetWarehouses()
        {
            var warehouseModelList =  warehouseRepo.List();
            var warehouseDtoList = this.mapper.Map<IEnumerable<WarehouseDto>>(warehouseModelList);
            return Ok(warehouseDtoList);
        }

        [HttpGet("{id}")]
        public ActionResult<WarehouseDto> GetWarehouseById(int id)
        {
            var warehouseModel =  warehouseRepo.GetById(id);
            var warehouseDto = this.mapper.Map<Warehouse, WarehouseDto>(warehouseModel);
            return Ok(warehouseDto);
        }

        [HttpGet("{id}/details")]
        public ActionResult<Warehouse> GetWarehouseByIdWithDetails(int id)
        {
            var specification = new WarehouseSpecification(wh => wh.Id == id);
            specification.IncludeStrings.Add("WarehouseOwneds.Owned");
            var warehouseModel = warehouseRepo.FirstOrDefault(specification);
            var warehouseAndOwningDto = this.mapper.Map<Warehouse, WarehouseAndOwningDto>(warehouseModel);
            return Ok(warehouseAndOwningDto);
        }

        [HttpGet("{id}/products")]
        public ActionResult<IEnumerable<ProductDto>> GetProductOfWarehouseById(int id)
        {
            var specification = new WarehouseOwnedSpecification(wo => wo.WarehouseId == id);
            specification.Includes.Add( wo => wo.Owned);
            var warehouseOwnedModelList = this.warehouseOwnedRepo.List(specification);
            var productModelList = warehouseOwnedModelList.Select(wom => wom.Owned);
            var productDtoList = this.mapper.Map<IEnumerable<Product>>(productModelList);
            return Ok(productDtoList);
        }

        [HttpDelete("{id}/products/{productId}")]
        public ActionResult<IEnumerable<ProductDto>> GetProductOfWarehouseById(int id, int productId)
        {

            var warehouseOwnedModel = this.warehouseOwnedRepo.List(wo => wo.OwnedId == productId && wo.WarehouseId == id).FirstOrDefault();
            if(warehouseOwnedModel != null) // (TODO TU)
            {
                this.warehouseOwnedRepo.Delete(warehouseOwnedModel);
            }
            return Ok();
        }
    }
}