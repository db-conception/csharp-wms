using System.Linq.Expressions;
using AutoMapper;
using CsharpWMS.Api.Dtos;
using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Domain.UseCases;
using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Core.Services;
using CsharpWMS.Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using CsharpWMS.Core.Application.Repositories;

namespace CsharpWMS.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BuysController : ControllerBase
    {
        private readonly IRepository<Buy> buyRepo;
        private readonly IMapper mapper;
        private readonly IMediator mediator;
        private readonly IBuyUseCases buyUseCase;

        public BuysController(
            IRepository<Buy> buyRepo,
            IMapper mapper,
            IMediator mediator,
            IBuyUseCases buyUseCase)
        {
            this.buyRepo = buyRepo;
            this.mapper = mapper;
            this.mediator = mediator;
            this.buyUseCase = buyUseCase;
        }

        [HttpGet]
        public ActionResult<BuyReadDto> GetBuys([FromQuery()]bool? paid = null, [FromQuery()]bool? received = null)
        {
            var buys = this.buyRepo.List(
                aBuy => 
                    (paid == null || (aBuy.IsPaid == paid)) &&
                    (received == null || (aBuy.IsReceived == received))
            );

            var buyDtos = this.mapper.Map<IEnumerable<BuyReadDto>>(buys);
            return Ok(buyDtos);
        }

        [HttpGet("{id}"), ActionName("Get Buy")]
        public ActionResult<BuyReadDto> GetBuy(int id)
        {
            var buy = this.buyRepo.GetById(id);
            var buyDto = this.mapper.Map<BuyReadDto>(buy);
            return Ok(buyDto);
        }

        [HttpPost]
        public ActionResult<BuyReadDto> CreateBuy([FromBody]BuyCreationDto buyCreationDto)
        {
            if(buyCreationDto != null)
            {
                var buyModel = this.mapper.Map<Buy>(buyCreationDto);
                this.buyRepo.Insert(buyModel);
                var buyDto = this.mapper.Map<BuyReadDto>(buyModel);
                return CreatedAtAction("Get Buy", new { Id = buyModel.Id }, buyDto);
            }

            return BadRequest();
        }

        [HttpPatch("{id}/pay")]
        public ActionResult<BuyReadDto> PayABuy(int id)
        {
            var buyModel = this.buyRepo.GetById(id);

            if(buyModel == null)
            {
                return NotFound();
            }

            if(buyModel.IsPaid){
                return Ok();
            }

            buyModel.IsPaid = true;
            this.buyRepo.Update(buyModel);
            var buyReadDto = this.mapper.Map<BuyReadDto>(buyModel);
            // TODO Trigger email asynchronusly using Mediator Design pattern
            return Ok(buyReadDto);
        }

        [HttpPatch("{id}/receive")]
        public async Task<ActionResult<BuyReadDto>> ReceiveABuy(int id)
        {
            var buyModel = this.buyRepo.GetById(id);

            if(buyModel == null)
            {
                return NotFound();
            }

            if(buyModel.IsReceived){
                return Ok();
            }

            await this.buyUseCase.ReceiveBuy(buyModel);

            var buyReadDto = this.mapper.Map<BuyReadDto>(buyModel);

            return Ok(buyReadDto);
        }
        
    }
}