using System.Threading.Channels;
using CsharpWMS.Core.Application.BackgroundServices;
using CsharpWMS.Core.Application.Mailing;
using CsharpWMS.Core.Application.Mediator;
using CsharpWMS.Core.Application.Repositories;
using CsharpWMS.Core.Domain.EventHandlers;
using CsharpWMS.Core.Domain.Models;
using CsharpWMS.Core.Services;
using CsharpWMS.Infrastructure;
using CsharpWMS.Infrastructure.Data;
using CsharpWMS.Infrastructure.Mailing;
using CsharpWMS.Infrastructure.Repositories;
using CsharpWMS.Core.Domain.UseCases;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

InfrastructureBuilder.InjectInfrastructureServices(builder);

builder.Services.AddScoped<ITransactionBuilder, TransactionBuilder>();
builder.Services.AddScoped<IRepository<Product>, Repository<Product>>();
builder.Services.AddScoped<IRepository<Buy>, Repository<Buy>>();
builder.Services.AddScoped<IRepository<Warehouse>, Repository<Warehouse>>();
builder.Services.AddScoped<IRepository<WarehouseOwned>, Repository<WarehouseOwned>>();
builder.Services.AddScoped<IProductUseCases, ProductUseCases>();
builder.Services.AddScoped<IBuyUseCases, BuyUseCases>();
builder.Services.AddSingleton<IEmailService, FakeEmailService>();

// Channels
builder.Services.AddSingleton<Channel<Email>>(Channel.CreateUnbounded<Email>(
    new UnboundedChannelOptions() { SingleReader = true }
));
builder.Services.AddSingleton<ChannelReader<Email>>(
    serviceProvider => serviceProvider.GetRequiredService<Channel<Email>>().Reader
);
builder.Services.AddSingleton<ChannelWriter<Email>>(
    serviceProvider => serviceProvider.GetRequiredService<Channel<Email>>().Writer
);

// HostedServices
builder.Services.AddHostedService<EmailSenderBackgroundService>();

builder.Services.AddSingleton<IWarehouseUseCases, WarehouseUseCases>();
builder.Services.AddSingleton<IMediator, HandmadeMediator>();
builder.Services.AddSingleton<BuyReceivedHandlerAddStockToWarehouse>();
builder.Services.AddSingleton<BuyReceivedHandlerSendEmail>();

// // EmailsBuilder
// builder.Services.AddSingleton<BuyReceivedEmailBuilder>();





var app = builder.Build();

if(app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();
MediatorInitalizer.Initalize(
    app
);
AppDbContextSeed.PrepPopulation(app, app.Environment.IsProduction());
app.MapGet("/", () => "Hello World!");
app.MapControllers();

app.Run();
