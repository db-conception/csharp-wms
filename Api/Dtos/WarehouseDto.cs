namespace CsharpWMS.Api.Dtos
{
    public class WarehouseDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}