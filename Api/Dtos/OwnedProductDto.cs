namespace CsharpWMS.Api.Dtos
{
    public class OwnedProductDto
    {
        /*
         * Id of the product
         */
        public int Id { get; set; }

        public string Sku { get; set; } 

        public string Name { get; set; }

        public double PriceSell { get; set; }

        public int Stock { get; set; }
    }
}