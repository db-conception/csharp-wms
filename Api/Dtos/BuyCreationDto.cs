namespace CsharpWMS.Api.Dtos
{
    public class BuyCreationDto
    {
        public int ProductId { get; set; }

        public int WarehouseId { get; set; }
        
        public bool IsPaid { get; set; }    

        public DateTime DateOfBuy { get; set; }

        public double PriceBuy { get; set; }

        public bool IsReceived { get; set; }

        public int Quantity { get; set; }
    }
}