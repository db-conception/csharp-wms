namespace CsharpWMS.Api.Dtos
{
    public class WarehouseAndOwningDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<OwnedProductDto> OwnedProducts { get; set; }
    }
}