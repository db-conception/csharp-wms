namespace CsharpWMS.Api.Dtos
{
    public class WarehouseCreationDto
    {
        public string Name { get; set; }
    }
}