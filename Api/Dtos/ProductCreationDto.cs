namespace CsharpWMS.Api.Dtos
{
    public class ProductCreationDto
    {
        public string Sku { get; set; } 

        public string Name { get; set; }

        public double PriceSell { get; set; }
    }
}