namespace CsharpWMS.Api.Dtos
{
    public class BuyReadDto
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public ProductDto Product { get; set; }

        public int WarehouseId { get; set; }
        
        public WarehouseDto Warehouse { get; set; }

        public bool IsPaid { get; set; }    

        public DateTime DateOfBuy { get; set; }

        public double PriceBuy { get; set; }

        public bool IsReceived { get; set; }

        public int Quantity { get; set; }
    }
}